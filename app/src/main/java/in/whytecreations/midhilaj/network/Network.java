package in.whytecreations.midhilaj.network;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface Network {

    @POST("api/register")
    Call<Frame> registerUser(@Body RequestBody body);

    @POST("api/search_business")
    Call<Frame> getBusiness(@Header("accept") String acceptStr, @Header("authorization") String authorizationStr,
                            @Body RequestBody body);


}
