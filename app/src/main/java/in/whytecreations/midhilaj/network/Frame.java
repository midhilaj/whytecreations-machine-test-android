package in.whytecreations.midhilaj.network;

import androidx.annotation.Keep;

import java.util.List;

import in.whytecreations.midhilaj.listingpage.model.BusincessModel;

@Keep
public class Frame {
    String status;
    List<BusincessModel> content;

    public void setContent(List<BusincessModel> content) {
        this.content = content;
    }

    public List<BusincessModel> getContent() {
        return content;
    }

    public Frame() {

    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    String message;
    //":"Your account has been successfully created, we have sent you a verification link to your mail. Check your email and using link verify your account."}
}
