package in.whytecreations.midhilaj;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.whytecreations.midhilaj.listingpage.ListingPageActivity;
import in.whytecreations.midhilaj.signup.callback.SignUpPresenterCallback;
import in.whytecreations.midhilaj.signup.presenter.SignUpPresenter;

public class SignUpActivity extends AppCompatActivity implements SignUpPresenterCallback {

    @BindView(R.id.sign_up_page_name)
    EditText mName;
    @BindView(R.id.sign_up_page_phone)
    EditText mPhone;
    @BindView(R.id.sign_up_page_email)
    EditText mEmail;
    @BindView(R.id.sign_up_page_password)
    EditText mPassword;
    @BindView(R.id.sign_up_page_sign_up_button)
    Button mSignUp;
    @BindView(R.id.sign_up_page_progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.sign_up_page_background_image)
    ImageView mBackGround;
    private SignUpPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } catch (Exception e) {

        }
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide(); //<< this
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        Glide.with(getApplicationContext()).load(R.drawable.image_bg_login).into(mBackGround);

    }


    @Override
    public void init() {
        mSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //checking any request on processing if not then call the registerUser method
                if (mProgressBar.getVisibility() == View.GONE) {
                    mPresenter.registerUser(mName.getText().toString().trim(), mPhone.getText().toString().trim(), mEmail.getText().toString().trim(), mPassword.getText().toString().trim());
                }
            }
        });
    }

    @Override
    public void setPhoneError(String message) {
        mPhone.setError(message);
        mPhone.requestFocus();
    }

    @Override
    public void setNameError(String message) {
        mName.setError(message);
        mName.requestFocus();
    }

    @Override
    public void setEmailError(String message) {
        mEmail.setError(message);
        mEmail.requestFocus();
    }

    @Override
    public void setPasswordError(String message) {
        mPassword.setError(message);
        mPassword.requestFocus();
    }

    @Override
    public void hideProgressBar() {
        if (mProgressBar.getVisibility() == View.VISIBLE) {
            mProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void showProgressBar() {
        if (mProgressBar.getVisibility() == View.GONE) {
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void registrationSuccessFul(String message) {
        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putBoolean(getString(R.string.auth), true).apply();
        startActivity(new Intent(SignUpActivity.this, ListingPageActivity.class));
    }

    @Override
    public void registrationFailed(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getBoolean(getResources().getString(R.string.auth), false) == true) {
            startActivity(new Intent(SignUpActivity.this, ListingPageActivity.class));
            finish();
        } else {
            mPresenter = new SignUpPresenter(this, this);
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter = null;
    }
}
