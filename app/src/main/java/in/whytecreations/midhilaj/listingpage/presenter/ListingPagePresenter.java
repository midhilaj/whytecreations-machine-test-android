package in.whytecreations.midhilaj.listingpage.presenter;

import android.app.Activity;

import in.whytecreations.midhilaj.listingpage.callback.ListingPagePresenterCallBack;
import in.whytecreations.midhilaj.listingpage.util.Util;
import in.whytecreations.midhilaj.listingpage.util.UtilNointerNetDialogCallBack;
import in.whytecreations.midhilaj.network.Frame;
import in.whytecreations.midhilaj.network.Network;
import in.whytecreations.midhilaj.network.RetrofitInstance;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListingPagePresenter {
    String assept = "application/json";
    String auth = "Bearer";
    Activity mActivity;
    ListingPagePresenterCallBack mCallBack;

    public ListingPagePresenter(Activity mActivity, ListingPagePresenterCallBack mListingPagePresenterCallBack) {
        mCallBack = mListingPagePresenterCallBack;
        this.mActivity = mActivity;
        getBisinessList();
    }

    private void getBisinessList() {
        if (Util.isNetworkAvailable(mActivity)) {
            mCallBack.showProgressBar();
            RequestBody body = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("user_id", "33")
                    .build();
            Network service = RetrofitInstance.getRetrofitInstance().create(Network.class);
            Call<Frame> call = service.getBusiness(assept, auth,
                    body);
            call.enqueue(new Callback<Frame>() {
                @Override
                public void onResponse(Call<Frame> call, Response<Frame> response) {

                    if (response.body() != null ? response.body().getContent() != null ? response.body().getContent().size() > 0 ? true : false : false : false) {
                        mCallBack.setResult(response.body().getContent());
                    } else {
                        mCallBack.onNoResult();
                    }

                    mCallBack.hideProgressBar();
                }

                @Override
                public void onFailure(Call<Frame> call, Throwable t) {
                    mCallBack.onFailure(t.getMessage() + "");
                    mCallBack.hideProgressBar();

                }
            });
        } else {
            Util.showNoInternetDialog(mActivity, new UtilNointerNetDialogCallBack() {
                @Override
                public void retry() {
                    getBisinessList();
                }
            });
        }

    }
}
