package in.whytecreations.midhilaj.listingpage.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import in.whytecreations.midhilaj.R;

public class Util {
    public static boolean isNetworkAvailable(Context context) {
        if (context != null) {
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        } else
            return false;
    }

    static AlertDialog mAlertDialog;
    static View mNoInternetAlertDialogView;

    public static void showNoInternetDialog(Activity activity,
                                            UtilNointerNetDialogCallBack callback) {
        if (mAlertDialog == null || !mAlertDialog.isShowing()) {
            Button mRetry;
            mNoInternetAlertDialogView =
                    LayoutInflater.from(activity).inflate(R.layout.no_internet_dialog_layout, null);
            mRetry = mNoInternetAlertDialogView.findViewById(R.id.no_internet_retry_button);
            mRetry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (mAlertDialog != null) {
                        if (mAlertDialog.isShowing()) {
                            mAlertDialog.dismiss();
                        }
                    }
                    callback.retry();
                }
            });
            mAlertDialog =
                    new AlertDialog.Builder(activity)
                            .setView(mNoInternetAlertDialogView).setCancelable(false)
                            .create();
            mAlertDialog.show();
        }
    }


}
