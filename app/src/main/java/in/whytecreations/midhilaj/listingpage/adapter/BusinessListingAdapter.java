package in.whytecreations.midhilaj.listingpage.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import in.whytecreations.midhilaj.R;
import in.whytecreations.midhilaj.listingpage.model.BusincessModel;

public class BusinessListingAdapter extends RecyclerView.Adapter<BusinessListingAdapter.MyViewHolder> {
    List<BusincessModel> mData;
    Context mContext;

    public BusinessListingAdapter(List<BusincessModel> mData) {
        this.mData = mData;
    }

    public List<BusincessModel> getData() {
        return mData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Glide.with(mContext).load(mData.get(position).getPhoto()).placeholder(R.drawable.image_placeholder).error(R.drawable.image_error).into(holder.mImage);
        holder.mTitle.setText(mData.get(position).getBusiness_name() + "");
        holder.mAddress.setText(mData.get(position).getLocations() + "");
        if (mData.get(position).getRating() != null) {
            //need support from backend to identify the logic
            holder.mRatingBar.setRating(1);
        }

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView mImage;
        TextView mTitle, mAddress;
        RatingBar mRatingBar;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            mRatingBar = itemView.findViewById(R.id.ratting_bar);
            mImage = itemView.findViewById(R.id.business_item_image_view);
            mTitle = itemView.findViewById(R.id.title_text_view);
            mAddress = itemView.findViewById(R.id.address_text_view);
        }
    }
}
