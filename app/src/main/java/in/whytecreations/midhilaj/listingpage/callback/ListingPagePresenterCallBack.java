package in.whytecreations.midhilaj.listingpage.callback;

import java.util.List;

import in.whytecreations.midhilaj.listingpage.model.BusincessModel;

public interface ListingPagePresenterCallBack {
    void setResult(List<BusincessModel> busincessModelList);

    void showProgressBar();

    void hideProgressBar();

    void showMessage(String message);

    void onFailure(String s);

    void onNoResult();
}
