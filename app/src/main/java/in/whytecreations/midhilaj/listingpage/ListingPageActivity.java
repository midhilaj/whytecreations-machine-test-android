package in.whytecreations.midhilaj.listingpage;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.whytecreations.midhilaj.R;
import in.whytecreations.midhilaj.listingpage.adapter.BusinessListingAdapter;
import in.whytecreations.midhilaj.listingpage.callback.ListingPagePresenterCallBack;
import in.whytecreations.midhilaj.listingpage.model.BusincessModel;
import in.whytecreations.midhilaj.listingpage.presenter.ListingPagePresenter;

public class ListingPageActivity extends AppCompatActivity implements ListingPagePresenterCallBack {
    ListingPagePresenter mPresenter;
    @BindView(R.id.business_listing_page_progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.business_listing_page_recycler_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.no_item_found_text_view)
    TextView mNoResultFound;
    BusinessListingAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_page);
        ButterKnife.bind(this);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new BusinessListingAdapter(new ArrayList<>());
        mRecyclerView.setAdapter(mAdapter);
        mPresenter = new ListingPagePresenter(this, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void setResult(List<BusincessModel> busincessModelList) {
        mAdapter.getData().addAll(busincessModelList);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void showProgressBar() {
        if (mProgressBar.getVisibility() == View.GONE) {
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideProgressBar() {
        if (mProgressBar.getVisibility() == View.VISIBLE) {
            mProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure(String message) {
        mNoResultFound.setVisibility(View.VISIBLE);
        showMessage(message);
    }

    @Override
    public void onNoResult() {
        mNoResultFound.setVisibility(View.VISIBLE);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter = null;
    }
}
