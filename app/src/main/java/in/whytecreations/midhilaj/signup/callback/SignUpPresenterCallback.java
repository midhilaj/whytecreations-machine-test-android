package in.whytecreations.midhilaj.signup.callback;

public interface SignUpPresenterCallback {
    void init();

    void setPhoneError(String message);

    void setNameError(String message);

    void setEmailError(String message);

    void setPasswordError(String message);

    void hideProgressBar();

    void showProgressBar();

    void registrationSuccessFul(String message);

    void registrationFailed(String message);
}
