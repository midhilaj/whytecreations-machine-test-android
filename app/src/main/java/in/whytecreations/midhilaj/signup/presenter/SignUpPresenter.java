package in.whytecreations.midhilaj.signup.presenter;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;

import in.whytecreations.midhilaj.listingpage.util.Util;
import in.whytecreations.midhilaj.listingpage.util.UtilNointerNetDialogCallBack;
import in.whytecreations.midhilaj.network.Frame;
import in.whytecreations.midhilaj.network.Network;
import in.whytecreations.midhilaj.network.RetrofitInstance;
import in.whytecreations.midhilaj.signup.callback.SignUpPresenterCallback;
import in.whytecreations.midhilaj.signup.model.SignUpModel;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpPresenter {
    SignUpModel mModel;
    String device_type = "any";
    String device_token = "fcm not in question";
    SignUpPresenterCallback mCallback;
    Activity mActivity;

    public SignUpPresenter(Activity mActivity, SignUpPresenterCallback mSignUpPresenterCallback) {
        mCallback = mSignUpPresenterCallback;
        this.mActivity = mActivity;

        mCallback.init();


    }

    public void setModel(SignUpModel mModel) {
        this.mModel = mModel;
    }

    public SignUpModel getModel() {
        return mModel;
    }

    public void registerUser(String name, String contact_no, String email, String password) {
        if (name.length() <= 2) {
            mCallback.setNameError("Name should contain at least 3 characters");
        } else if (contact_no.length() <= 4 || contact_no.length() > 15) {
            mCallback.setPhoneError("Invalid Mobile number. Number should be greater than  4 and less than 15");
        } else if (isValidEmail(email) != true) {
            mCallback.setEmailError("Invalid Email id");

        } else if (password.length() <= 5) {
            mCallback.setPasswordError("Invalid password minimum 6 characters required");
        } else if (password.length() > 20) {
            mCallback.setPasswordError("Invalid password maximum 19 characters are allowed ");
        } else {
            mCallback.showProgressBar();
            if (Util.isNetworkAvailable(mActivity)) {
                RequestBody userRegistration = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("name", name)
                        .addFormDataPart("email", email)
                        .addFormDataPart("contact_no", contact_no)
                        .addFormDataPart("password", password)
                        .addFormDataPart("device_type", device_type)
                        .addFormDataPart("device_token", device_token)
                        .build();
                Network service = RetrofitInstance.getRetrofitInstance().create(Network.class);
                Call<Frame> call = service.registerUser(
                        userRegistration);
                call.enqueue(new Callback<Frame>() {
                    @Override
                    public void onResponse(Call<Frame> call, Response<Frame> response) {
                        mCallback.hideProgressBar();
                        if (response.body().getStatus().equals("success")) {
                            mCallback.registrationSuccessFul(response.body().getMessage());
                        } else {
                            mCallback.registrationFailed(response.body().getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<Frame> call, Throwable t) {
                        Log.i("onFailure", "yes  " + t.getMessage());
                        mCallback.hideProgressBar();
                        mCallback.registrationFailed("Registration failed. Try with other account details");
                    }
                });
            } else {
                Util.showNoInternetDialog(mActivity, new UtilNointerNetDialogCallBack() {
                    @Override
                    public void retry() {
                        registerUser(name, contact_no, email, password);
                    }
                });
            }


        }
    }

    boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }
}
